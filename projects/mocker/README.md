# Mocker
Library for creating mocks for your projects.

## Add a dependency and script to your `package.json`:
```
npm install @junte/mocker
```

## Change a `tsconfig.json`:
```
{
    ...
    compilerOptions: {
        ...
        emitDecoratorMetadata: true,
        ...
    },
    ...
}
```

# Examples

Model `ObjectLink.ts:`
```
@MockModel()
export class ObjectLink {

  @MockField({mock: 1})
  id: number;

  @MockField({mock: 'Test title'})
  title: string;
  
}
```

Model `User.ts:`
```
@MockModel()
export class User {

  @MockField({mock: () => 123})
  id: number;
  
  @MockField({mock: () => 'Test nick'})
  nick: string;

  @MockField({mock: false})
  approved: boolean;

  @MockField({mock: ObjectLink})
  name: ObjectLink;

  @MockField({
    name: 'first_name',
    mock: 'First name'
  })
  firstName: string;

  @MockField({
    serializer: new ArraySerializer(new ModelSerializer(ObjectLink)),
    mock: {type: ObjectLink, length: 2}
  })
  hobby: ObjectLink[];

}
```
at the output we get
`object_link.json:`
```
{
    "id": 1,
    "title": "Test title"
}
```

and `user.json:`

```
{
    "id": 123,
    "nick": 'Test nick',
    "approved": false,
    "name": {
        "id": 1,
        "title": "Test title"
    },
    "first_name": "First name",
    "hobby": [
        {
            "id": 1,
            "title": "Test title"
        },
        {
            "id": 1,
            "title": "Test title"
        }
      ...
    ]
}
```
