import 'reflect-metadata';

export const MOCK_MODEL_METADATA_KEY = 'mock_model_metadata';
export const MOCK_FIELDS_METADATA_KEY = 'mock_fields_metadata';

export type MockFieldConfig = boolean | boolean[] | number | number[]
  | string | string[] | (() => any) | object | { type: new () => object, length: number };

export function MockField(config: MockFieldConfig) {
  return (obj: object, property: string | symbol) => {
    const metadata = Reflect.getMetadata(MOCK_FIELDS_METADATA_KEY, obj) || {};
    const type = Reflect.getMetadata('design:type', obj, property);
    metadata[property] = {type, config};
    Reflect.defineMetadata(MOCK_FIELDS_METADATA_KEY, metadata, obj);
  };
}

export function MockModel(callback: (obj: object, context?: object, index?: number) => void) {
  return (constructor: new () => object) => {
    Reflect.defineMetadata(MOCK_MODEL_METADATA_KEY, callback, constructor.prototype);
  };
}
