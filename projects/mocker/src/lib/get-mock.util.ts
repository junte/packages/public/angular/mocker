import { MOCK_FIELDS_METADATA_KEY, MOCK_MODEL_METADATA_KEY } from './decorators';

type Constructor<T> = new () => T;
type Activator<T> = () => Constructor<T>;

const MAX_LEVELS = 5;

type call = (context: object, index: number, level: number) => void;

export function getMock<T>(model: Constructor<T> | Activator<T>,
                           context: object = {}, index: number = 0, level: number = 0): T {
  const next = level + 1;
  const obj = !!model.prototype ? new (model as Constructor<T>)() as T : new ((model as Activator<T>)())();
  const metadata = Reflect.getMetadata(MOCK_FIELDS_METADATA_KEY, obj);
  for (const property in metadata) {
    const {type, config} = metadata[property];
    if (!type) {
      continue;
    }
    if (type === Boolean || type === Number || type === String || type === Date || type === Object) {
      if (config !== undefined) {
        if (typeof config === 'function') {
          obj[property] = (config as call)(context, index, next);
        } else {
          obj[property] = config;
        }
      }
    } else if (type === Array) {
      if (config !== undefined) {
        if ('type' in config) {
          const conf = config as { type: new () => object, length: number };
          const list = [];
          for (let i = 0; i < conf.length; i++) {
            if (level < MAX_LEVELS) {
              list.push(getMock(conf.type, context, i, next));
            }
          }
          obj[property] = list;
        } else {
          if (typeof config === 'function') {
            obj[property] = (config as call)(context, index, next);
          } else {
            obj[property] = config;
          }
        }
      }
    } else {
      if (config !== undefined) {
        if (config === type) {
          if (level < MAX_LEVELS) {
            obj[property] = getMock(config, context, index, next);
          }
        } else if (typeof config === 'function') {
          obj[property] = (config as call)(context, index, next);
        } else {
          throw `Wrong mock type for property [${property}]`;
        }
      }
    }
  }
  const callback = Reflect.getMetadata(MOCK_MODEL_METADATA_KEY, obj);
  if (!!callback) {
    callback(obj, context, index);
  }

  return obj;
}
