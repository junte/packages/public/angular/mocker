export enum Language {
  en = 'en',
  ru = 'ru'
}

export function detectLanguage() {
  const base = document.querySelector('base')
    .getAttribute('href');
  switch (base) {
    case '/ru/':
      return Language.ru;
    case '/en/':
    default:
      return Language.en;
  }
}
