import { ArraySerializer, PrimitiveSerializer, Serializer } from 'serialize-ts';
import { ModelMetadataSerializer } from 'serialize-ts/dist/serializers/model-metadata.serializer';
import { faker, field, mocks, model } from 'src/app/decorators';

enum Test {
  test = 'test'
}

export class KeyValueSerializer implements Serializer<{ [key: string]: string }> {
  serialize(obj: { [key: string]: string }): { [key: string]: string } {
    return JSON.parse(JSON.stringify(obj));
  }

  deserialize(src: { [key: string]: string }): { [key: string]: string } {
    return JSON.parse(JSON.stringify(src));
  }
}


@model()
export class ObjectLink {

  @field({mock: () => mocks.random(1, 100)})
  id: number;

  @field({
    mock: () => faker.helpers.randomize([
      'Presentation 1',
      'Presentation 2',
      'Presentation 3'
    ])
  })
  presentation: string;
}

@model()
export class User {

  @field({mock: faker.random.number({max: 100})})
  id: number;

  @field({mock: faker.random.boolean()})
  approved: boolean;

  @field({mock: ObjectLink})
  presentation: ObjectLink;

  @field({serializer: new KeyValueSerializer(), mock: {apples: 5, bananas: 10}})
  fruits: { [key: string]: number };

  @field({
    name: 'first_name',
    mock: () => faker.helpers.randomize([
      'Name 1',
      'Name 2',
      'Name 3'
    ])
  })
  firstName: string;

  @field({
    serializer: new ArraySerializer(new PrimitiveSerializer()),
    mock: [Test.test]
  })
  test: Test[];

  @field({
    serializer: new ArraySerializer(new ModelMetadataSerializer(ObjectLink)),
    mock: {type: ObjectLink, length: 10}
  })
  hobby: ObjectLink[];
}
